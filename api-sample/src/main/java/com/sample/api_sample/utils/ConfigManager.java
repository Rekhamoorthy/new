package com.sample.api_sample.utils;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
public class ConfigManager {
	private static final Properties prop = new Properties();
	private ConfigManager() throws IOException{
		FileInputStream inputStream = new FileInputStream ("/com.api-sample/resources/config.properties");
		prop.load(inputStream);
	}
public String getString(String key) {
	return System.getProperty(key, prop.getProperty(key));
}
}
