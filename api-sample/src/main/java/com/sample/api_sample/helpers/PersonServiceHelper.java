package com.sample.api_sample.helpers;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

import java.lang.reflect.Type;
import java.util.List;

import org.apache.http.HttpStatus;

import com.fasterxml.jackson.core.type.TypeReference;
import com.sample.api_sample.constant.EndPoints;
import com.sample.api_sample.models.Post;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;


public class PersonServiceHelper {
	private static final String BASE_URL = "http://localhost:3000";
    public PersonServiceHelper() {
   RestAssured.baseURI =BASE_URL;
	RestAssured.useRelaxedHTTPSValidation();
   }
public List<Post> getAllPost(){
	Response response = RestAssured
			.given().log().all()
			.contentType(ContentType.JSON)
			.get(EndPoints.GET_ALL_POST)
			.andReturn();
	response.prettyPrint();
	Type type = new TypeReference<List<Post>>(){}.getType();
	assertEquals(response.statusCode(),HttpStatus.SC_OK,"Ok");
	List<Post> postList = response.as(type);

	return postList;
}


public Response createPost() {
	Post post = new Post();
	post.setId(3);
	post.setTitle("BRAVE NEW WORLD ");
	post.setAuthor(" ALDOUS HUXLEY");
	Response response = RestAssured
			.given()
			.contentType(ContentType.JSON)
			.when()
			.body(post)
			.post(EndPoints.CREATE_POST)
			.andReturn();
	assertEquals(response.statusCode(),HttpStatus.SC_CREATED,"Created");
	response.prettyPrint();
	return response;
}
public Response patchPost(Integer id) {
	Post post = new Post();
	post.setTitle(" COLD COMFORT FARM ");
	post.setAuthor("STELLA GIBBONS");
	Response response = RestAssured
			.given()
			.contentType(ContentType.JSON)
			.pathParam("id", id)
			.when()
			.body(post)
			.patch(EndPoints.UPDATE_POST)
			.andReturn();
	assertTrue(response.statusCode()==HttpStatus.SC_OK);
	response.prettyPrint();
	return response;
}
public Response deletePost(Integer id) {
	Response response = RestAssured
			.given()
			.contentType(ContentType.JSON)
			.pathParam("id", id)
			.delete(EndPoints.DELETE_POST)
			.andReturn();
	assertTrue(response.statusCode()== HttpStatus.SC_OK);
	return response;
}
public Response getSinglePost(){
	Response response = RestAssured
			.given()
			.pathParam("id","2" )
			.contentType(ContentType.JSON)
			.get(EndPoints.GET_SINGLE_POST)
			.andReturn();
	response.prettyPrint();
	assertEquals(response.statusCode(),HttpStatus.SC_OK,"Ok");
	return response;
	
}
}
