package com.sample.api_sample.test;

import java.util.List;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertNotNull;


import com.sample.api_sample.helpers.PersonServiceHelper;
import com.sample.api_sample.models.Post;
import io.restassured.response.Response;


public class TestGETPerson {
private PersonServiceHelper personServiceHelper;
@BeforeClass
public void init()
{
	personServiceHelper = new PersonServiceHelper();
	
}
@Test
public void testGetAllPost()
{
	
	List<Post> postList = personServiceHelper.getAllPost();
	assertNotNull(postList,"Post List is not empty");
	assertFalse(postList.isEmpty(),"Post List is not true");
}
@Test
public void testGetSinglePost()
{
	
	Response postList = personServiceHelper.getSinglePost();
	assertNotNull(postList,"Post List is not empty");
}

}
