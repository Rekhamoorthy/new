package com.sample.api_sample.test;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.sample.api_sample.helpers.PersonServiceHelper;

public class TestDELETEPerson {
	private PersonServiceHelper personServiceHelper;
	@BeforeClass
	public void init() {
		personServiceHelper = new PersonServiceHelper();
	}
	
	@Test
	public void deletePost() {
		personServiceHelper.deletePost(3);
	}

}
