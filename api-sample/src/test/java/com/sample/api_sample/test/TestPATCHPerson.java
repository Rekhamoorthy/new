package com.sample.api_sample.test;

import static org.testng.Assert.assertNotNull;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.sample.api_sample.helpers.PersonServiceHelper;

public class TestPATCHPerson {
private PersonServiceHelper personServiceHelper;
	
	@BeforeClass
	public void init() {
		personServiceHelper = new PersonServiceHelper();
	}
	
	@Test
	public void patchPost() {
		String id = personServiceHelper.patchPost(2).jsonPath().getString("id");
		System.out.println(id);
		assertNotNull(id,"Updated");
	}
}
